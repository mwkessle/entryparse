r"""
This is code that is designed to parse an input string and extract 
mentions, emoticons, and links, and it then outputs said mensions, 
emoticons, and links as a JSON string.  The code works with either
Python 2.7 or Python 3.4.

Files:
- README - This is it.
- __init__.py - Very simple module.
- EntryParser.py - Code to parse string.
- EntryParserTest.py - Test cases to test the above code.


An example of using this code to parse a string is as follows:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#!/bin/env python

from entryparse.EntryParser import EntryParser

parser = EntryParser()
output = parser.parse("@Atlassian (wow) Nice digs! http://www.kvue.com/story/tech/2015/04/14/atlassians-new-austin-home/25801279/")
print ( "JSON String: \n%s"%(output,))

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The above results in:
JSON String:
{
  "mentions": [
    "Atlassian"
  ], 
  "emoticons": [
    "wow"
  ], 
  "links": [
    {
      "url": "http://www.kvue.com/story/tech/2015/04/14/atlassians-new-austin-home/25801279/", 
      "title": "Atlassian's new Austin home"
    } 
  ]
}
"""


