#!/bin/env python

import json
import re
import types
import certifi
import six
from lxml.html import fromstring
from urllib3.util.url import parse_url,Url
from multiprocessing.pool import Pool
from urllib3 import PoolManager
from urllib3.util.retry import Retry
from urllib3.util.timeout import Timeout
from urllib3.exceptions import ConnectTimeoutError, MaxRetryError, ProtocolError, ReadTimeoutError

def get_title( maybe_url ):
    """
    Helper function to get the title for the string URL that is passed into it.
    Currently not a part of the EntryParse class because the multi-threaded 
    function must be serializable and a bound method is not easily serializable.
    
    input:
     - maybe_url (string): 

    """
    url=None
    supported_schemes = ["http", "https", "HTTP", "HTTPS"]
    url_tup = parse_url( maybe_url )

    if ((url_tup.netloc is not None) and 
        (url_tup.scheme is not None) and
        (url_tup.scheme in supported_schemes)): 
        # Jump through hoops incase scheme is either HTTP or HTTPS
        new_tup = Url(url_tup.scheme.lower(), url_tup.auth, url_tup.host, 
                      url_tup.port, url_tup.path, url_tup.query, 
                      url_tup.fragment)
        normal_url = new_tup.url

        # TBD Should validate against a database
        # of known malicious web-sites.
        # perhaps malwaredomainlists.com or
        # google's safe browsing
        
        cert_reqs = None
        ca_certs = None
            
        # for https require Certs
        # default is to not
        if new_tup.scheme == 'https':
            cert_reqs = 'CERT_REQUIRED'
            ca_certs = certifi.where()

        # Max timeout for a slow server 5 seconds
        # Max Retries for a down or non-existent server=5, read=2, redirects=2
        con = PoolManager( cert_reqs = cert_reqs,
                           ca_certs = ca_certs,
                           timeout = Timeout(5),
                           retries = Retry(connect=5, read=2, redirect=2))
        try:
            resp = con.request("GET", normal_url)
        except MaxRetryError:
            return None
        except ProtocolError:
            return None
        except ConnectTimeoutError:
            return None
        except ReadTimeoutError:
            return None
        con.clear()
        parsed_resp = fromstring(resp.data)
        # Find title in response
        title = parsed_resp.find(".//title")

        if title is not None:
            ## Only add to Links if we get a title back
            ## 
            url = {}
            url['url'] = normal_url
            url['title'] = title.text

    return url

class EntryParser():
    """
    Class to handle parsing input string and extracting any of the following: 
    mentions, emoticons, and http(s) URLs

    @Dependencies certifi, lxml, json, six, urllib3

    Attributes:

        ret (dictionary): holds the metions, emoticons and links lists.
        possible_urls (list): list of tokens that matched PURL_re expression
                              when the input string is parsed.
                              This list of tokens is passed to get_title to 
                              retrieve the title(s) of the webpage(s) they 
                              reference.  If the url is not an http reference 
                              nothing is retrieved.
        CHAR_STRING_re (regex): Character string regular expression.
        EMOTICON_re (regex): Emoticon regular expression.
        MENTION_re (regex): Mention regular expression.
        PURL_re (regex): Possible URL regular expression.
        WHITESPACE_re (regex): Whitespace regular expression.
        entry_reg_ex (regex): Compiled regular expression that combines
                              the _re attributes and is used by scanner to 
                              tokenize the input string.
        handle_token (dictionary): A dictionary that maps the token name to 
                                   a method to process the token value.
    """

    def __init__(self):
        
        self.ret = {}
        self.possible_urls = []
        self.CHAR_STRING_re = r'(?P<CHAR_STRING>[\S]+)'
        self.EMOTICON_re = r'(?P<EMOTICON>\([a-zA-Z]{1,15}\))'
        self.MENTION_re = r'(?P<MENTION>@[a-zA-Z]+)'
        self.PURL_re = r'(?P<PURL>[a-zA-Z0-9]+:[\S]+)'
        self.WHITESPACE_re = r'(?P<WHITESPACE>[\s]+)'

        #
        # IMPORTANT NOTE: Order of regexs in the following 
        #                 join sets the precedence.
        #  
        self.entry_reg_ex = re.compile('|'.join([self.WHITESPACE_re,
                                                 self.PURL_re,
                                                 self.MENTION_re,
                                                 self.EMOTICON_re,
                                                 self.CHAR_STRING_re]))

        self.handle_token = {'CHAR_STRING': self._handle_char_string,
                             'EMOTICON': self._handle_emoticon, 
                             'MENTION': self._handle_mention, 
                             'PURL': self._handle_purl, 
                             'WHITESPACE': self._handle_whitespace,
                              }

    def _handle_char_string(self, t):
        """Ignore random character strings"""
        pass

    def _handle_emoticon(self, t):
        """Appends a token's value to the internal emoticon list"""
        if isinstance(t, six.string_types):
            try:
                self.ret['emoticons'].append(t[1:len(t)-1])
            except KeyError:
                self.ret['emoticons'] = [t[1:len(t)-1]]
        
    def _handle_mention(self, t):
        """Appends a token's value to the internal mention list"""
        if isinstance(t, six.string_types):
            try:
                self.ret['mentions'].append(t[1:])
            except KeyError:
                self.ret['mentions'] = [t[1:]]
                
    def _handle_purl(self, t):
        """ 
        Adds possible urls to internal possible_urls list used to generate 
        the list of links output as part of the JSON output by parse.
        """        
        if isinstance(t, six.string_types):
            self.possible_urls.append(t)

    def _handle_whitespace(self, t):
        """Ignore all whitespace"""
        pass

    def parse(self, entry, indent=2):
        """
        Parses a string and outputs a JSON string containing the 
        mentions, emoticons, and links.

        input:
         - entry (string): A string that is to be parsed
         - indent (int): This set the indent level for the format string.  
                         If the values is 0 then there will no formatting,
                         and the string output will be a compact (single-line) 
                         JSON string. (Default = 2)
        examples:
        parse("@chris you around?")
        Return (string):
        {
          "mentions": [
          "chris"
          ]
        }

        parse("Good morning! (megusta) (coffee)")
        Return (string):
        {
          "emoticons": [
          "megusta",
          "coffee"
          ]
        }

        Input: "Olympics are starting soon; http://www.nbcolympics.com"
        Return (string):
        {
          "links": [
                     {
                       "url": "http://www.nbcolympics.com",
                       "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
                     }
                   ]
        }

        parse("@bob @john (success) such a cool feature; 
                https://twitter.com/jdorfman/status/430511497475670016")
        Return (string):
        {
          "mentions": [
            "bob",
            "john"
          ],
          "emoticons": [
            "success"
          ]
          "links": [
            {
              "url": "https://twitter.com/jdorfman/status/430511497475670016",
              "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
            }
          ]
        }
        """
        if not (isinstance(entry, six.string_types)):
            raise TypeError(" %s object is not parsable "% type(entry))
        self.ret = {}
        self.possible_urls = []

        # tokenized and handled
        scanner = self.entry_reg_ex.scanner(entry)
        for token in iter(scanner.match, None):
            self.handle_token[token.lastgroup](token.group())

        # Get url titles
        # Thread Pool to retrieve URL titles
        num_tthreads = min( 4, len( self.possible_urls ))
        if num_tthreads > 0:
            title_pool = Pool(num_tthreads)

            # Multi-threaded request the titles
            links_list = title_pool.map(get_title, self.possible_urls)

            # get_title could return None if the URL is malformed or the 
            # website doesn't answer
            links_list = [x for x in links_list if x is not None]
            if len (links_list) > 0:
                self.ret['links'] = links_list
            title_pool.close()
            
        json_ret = json.JSONEncoder(indent=indent).encode(self.ret)
        return json_ret

if __name__ == "__main__": 
    qparser = EntryParser()
    qresult = qparser.parse("(tada) (doh) https://twitter.com/jdorfman/" +
                            "status/430511497475670016 a b @this " + 
                            "mattwkessler@gmail.com @ doh http:// " +
                            "http:/// http://// http://www.google.com"*1)
    print ( "%s"%(qresult,))
