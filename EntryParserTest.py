#!/bin/env python

import logging
import sys
import unittest
from EntryParser import get_title,EntryParser
from json import loads, JSONEncoder


def ordered(obj):
    """Borrowed code from 
    http://stackoverflow.com/questions/25851183/how-to-compare-two-json-objects-with-the-same-elements-in-a-different-order-equa

    Somewhat nodeterministic the order of the JSON string encoding.
    """
    if isinstance(obj, dict):
        return sorted((k, ordered(v)) for k, v in obj.items())
    if isinstance(obj, list):
        return sorted(ordered(x) for x in obj)
    else:
        return obj

class GetTitleTest(unittest.TestCase):
    def test_none (self):
        result = get_title(None)
        self.assertEqual(result, None)

    def test_malformed_1 (self):
        result = get_title("http:////")
        self.assertEqual(result, None)

    def test_malformed_2 (self):
        result = get_title("http://www")
        self.assertEqual(result, None)

    def test_malformed_3 (self):
        result = get_title("asdf")
        self.assertEqual(result, None)

    def test_no_scheme (self):
        result = get_title("://sdf")
        self.assertEqual(result, None)

    def test_unsupported_scheme (self):
        result = get_title("ftp://ftp.google.com")
        self.assertEqual(result, None)

    def test_input_number (self):
        with self.assertRaises(TypeError):
            result = get_title(11234)
            
    def test_valid (self):
        result = get_title("http://www.google.com")
        correct_ans = {'url': 'http://www.google.com', 'title' : 'Google' }
        self.assertEqual(result, correct_ans)

    def test_valid_caps (self):
        result = get_title("HTTP://www.google.com")
        correct_ans = {'url': 'http://www.google.com', 'title' : 'Google' }
        self.assertEqual(result, correct_ans)

    def test_valid_https (self):
        result = get_title("https://www.google.com")
        correct_ans = {'url': 'https://www.google.com', 'title' : 'Google' }
        self.assertEqual(result, correct_ans)
    
    def test_valid_https_caps (self):
        result = get_title("HTTPS://www.google.com")
        correct_ans = {'url': 'https://www.google.com', 'title' : 'Google' }
        self.assertEqual(result, correct_ans)

    # I draw the line at HttPs URLs
    def test_invalid_scheme_mixed_case(self):
        result = get_title("HttPS://www.google.com")
        correct_ans = None
        self.assertEqual(result, correct_ans)

    def test_mismatched_scheme_port(self):
        result = get_title("http://www.google.com:443")
        correct_ans = None
        self.assertEqual(result, correct_ans)


class EntryParserTest(unittest.TestCase):

    def test_handle_emoticon(self):
        parser = EntryParser()
        parser._handle_emoticon("(blah)")
        self.assertEqual(parser.ret, {"emoticons":["blah"]})

    def test_handle_emoticon_num(self):
        parser = EntryParser()
        parser._handle_emoticon(12345)
        self.assertEqual(parser.ret, {})

    def test_handle_mention_num(self):
        parser = EntryParser()
        parser._handle_mention(12345)
        self.assertEqual(parser.ret, {})

    def test_handle_mention_num(self):
        parser = EntryParser()
        parser._handle_mention("@you")
        self.assertEqual(parser.ret, {"mentions":["you"]})

    def test_handle_purl(self):
        parser = EntryParser()
        parser._handle_purl("doh")
        self.assertEqual(parser.possible_urls, ["doh"])

    def test_handle_purl_num(self):
        parser = EntryParser()
        parser._handle_purl(67890)
        self.assertEqual(parser.possible_urls, [])

    def test_single_emoticon(self):
        parser = EntryParser()
        result = parser.parse("(Single)")
        correct_ans = JSONEncoder(indent=2).encode({ "emoticons": ["Single"]})
        self.assertEqual( correct_ans, result )

    def test_multiple_emoticon(self):
        parser = EntryParser()
        result = parser.parse("(first) (second) asdf (third)")
        correct_ans = {"emoticons": ["first","second","third"]}
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_single_mention(self):
        logger = logging.getLogger("test_debug")
        parser = EntryParser()
        result = parser.parse("@SingleMention")
        correct_ans = { "mentions": ["SingleMention"]}
        logger.debug("Here's parser return printed %s %s %s" % 
                     (JSONEncoder().encode(parser.ret), 
                      type(correct_ans), 
                      type (parser)))
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_multiple_mentions(self):
        parser = EntryParser()
        result = parser.parse("@testThis @ME")
        correct_ans = { "mentions": [ "testThis", "ME" ] }
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_mentions_and_reserved_characters(self):
        parser = EntryParser()
        result = parser.parse(" \ \@theis /blah @ @this")
        correct_ans = { "mentions": ["this"] }
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_mentions_and_email(self):
        parser = EntryParser()
        result = parser.parse("@ @this mattwkessler@gmail.com @ doh")
        correct_ans = { "mentions": ["this"] }
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_mentions_at_beginning(self):
        parser = EntryParser()
        result = parser.parse("@this mattwkessler@gmail.com @ doh")
        correct_ans = { "mentions": ["this"] }
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_mentions_and_http_url(self):
        parser = EntryParser()
        result = parser.parse("a b @this mattwkessler@gmail.com @ doh"+
                              "http:// http:/// http://// "+
                              "http://www.google.com:80")
        correct_ans = {"mentions": ["this"],
                       "links":[{"url" : "http://www.google.com:80",
                                 "title" : "Google"}]}
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_mentions_and_https_url(self):
        parser = EntryParser()
        result = parser.parse("a b @this mattwkessler@gmail.com @ doh"+
                              "https:// http:/// http://// "+
                              "https://www.google.com")
        correct_ans = { "mentions": ["this"], 
                        "links":[ { "url" : "https://www.google.com",
                                    "title" : "Google"}]}
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_mentions_and_down_server(self):
        parser = EntryParser()
        result = parser.parse("@this https://www.overenginerd.com")
        correct_ans = {"mentions": ["this"]}
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_emu(self):
        parser = EntryParser()
        result = parser.parse("a b (happy) @this mattwkessler@gmail.com @ doh"+
                              "https:// http:/// (sad) http://// "+
                              "https://www.google.com (bewildered)")
        correct_ans = {"mentions": ["this"], 
                       "links":[ { "url" : "https://www.google.com",
                                   "title" : "Google"} ],
                       "emoticons":["happy", "sad", "bewildered"]}
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))

    def test_reuse(self):
        parser = EntryParser()
        result = parser.parse("a b (happy) @this mattwkessler@gmail.com @ doh"+
                              "https:// http:/// (sad) http://// "+
                              "https://www.google.com (bewildered)")
        correct_ans = {"mentions": ["this"], 
                       "links":[ { "url" : "https://www.google.com",
                                   "title" : "Google"} ],
                       "emoticons":["happy", "sad", "bewildered"]}
        self.assertEqual(ordered(parser.ret), ordered(correct_ans))
        result2 = parser.parse("a b (veryhappy) @thiss mattwkessler@gmail.com @ doh"+
                               "https:// http:/// (verysad) http://// "+
                               "https://www.google.com (verybewildered)")
        correct_ans2 = {"mentions": ["thiss"], 
                        "links":[ { "url" : "https://www.google.com",
                                    "title" : "Google"} ],
                        "emoticons":["veryhappy", "verysad", "verybewildered"]}
        self.assertEqual(ordered(parser.ret), ordered(correct_ans2))
        
if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger("test_debug").setLevel(logging.INFO)
    unittest.main()
